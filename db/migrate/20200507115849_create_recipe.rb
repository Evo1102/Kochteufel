class CreateRecipe < ActiveRecord::Migration[6.0]
  def change
    create_table :recipes do |t|
      t.string :name, null: false
      t.string :author, null: false
      t.string :ingredients, null: false
      t.string :preparation, null: false
      t.timestamps
    end
  end
end
