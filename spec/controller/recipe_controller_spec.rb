require 'simplecov'
SimpleCov.start
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RecipesController, type: :controller do
  let(:params) do
    {
      name: 'Burgerrole Low Carb',
      author: 'Dennis',
      ingredients: 'low-fat curd, eggs, backing powder',
      preparation: 'Mix the ingredients in a bowl to on mass.'
    }
  end
  let(:recipe) { Recipe.last }

  context 'testing the create method with valid params' do
    before do
      Recipe.create(params)
    end
    let(:subject) { post :create, params: params }

    context 'should create an interested person' do
      it 'with params' do
        subject
        expect(recipe.reload).to have_attributes params
        expect(recipe.created_at).to eql recipe.updated_at
      end
    end
  end
end
