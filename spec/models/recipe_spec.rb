require 'simplecov'
SimpleCov.start
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipe, type: :model do
  context 'testing the new Method' do
    let(:subject) { Recipe.new }
    let(:attributes) do
      {
        id: nil,
        name: nil,
        author: nil,
        ingredients: nil,
        preparation: nil,
        created_at: nil,
        updated_at: nil
      }
    end

    it 'will create a new recipe object' do
      expect(subject).to have_attributes attributes
    end
  end

  context 'testing the search method' do
    let(:name) { 'beef' }
    let(:author) { 'dennis' }
    let(:ingredients) { 'beef' }
    let(:subject) { Recipe.all }
    let(:params) do
      {
        name: 'Beef Wellington',
        author: 'Dennis Koop',
        ingredients: 'beef, beer, butter',
        preparation: 'cook it'
      }
    end

    before do
      Recipe.create(params)
    end

    xit 'it will return the objects which where searched' do
      expect(subject.search(name).name).to include name
      expect(subject.search(ingredients).name).to include name
      expect(subject.search(author).name).to include name
    end
  end
end
