# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get 'logout' => 'devise/sessions#destroy'
    get 'login' => 'devise/sessions#new'
  end

  get 'recipes' => 'recipes#index'
  get 'recipe/new' => 'recipes#new'
  get 'recipe/show/:id' => 'recipes#show'
  post 'recipe/create' => 'recipes#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'start#index'
end
