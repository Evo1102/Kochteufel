# config valid for current version and patch releases of Capistrano
set :application, 'devilskitchen'
set :repo_url, 'git@gitlab.com:Evo1102/Kochteufel.git'
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :log_level, :info
set :keep_releases, 5
set :bundle_binstubs, -> { shared_path.join('bin') }
set :bundle_flags, '--deployment --quiet'
set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, File.read('.ruby-version').strip
set :rbenv_map_bins, %w(rake gem bundle ruby rails)
set :rbenv_roles, :all # default value
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

require 'term/ansicolor'
namespace :deploy do
  task :deployment_start do
    on roles(:all) do |host|
      puts "#{Term::ANSIColor.yellow}[INFO] capistrano log_level: #{fetch(:log_level)} " \
           "(you can change this in #{fetch(:rails_env)}.rb) #{Term::ANSIColor.reset}"
      puts "#{Term::ANSIColor.green}[INFO] Starting deployment to host: #{host}#{Term::ANSIColor.reset}"
    end
  end

  task :deployment_finished do
    puts "#{Term::ANSIColor.green}[INFO] Deployment finished. " \
         "Please check if everything is ok at: bayerische with environment #{fetch(:rails_env)}#{Term::ANSIColor.reset}"
  end

  before  :starting, :deployment_start
  after   :publishing, :restart
  # after   :restart, :write_revision
  # after   :write_revision, :deployment_finished
  after   :restart, :deployment_finished
end

# use this method in a specific environment deploy script
# (e.g config/deploy/stage.rb) to make sure, that only the
# correct branch is deployed to the environment
def check_if_branch_is_correct(branch)
  return if ENV['CI']
  rails_env = fetch(:rails_env)
  puts "\nChecking, if this is the #{branch} branch"
  return if current_branch.gsub(/-.*$/, '') == branch
  puts "\nATTENTION: you are only allowed to deploy the '#{branch}' branch to #{rails_env}\n"
  exit(1)
end

def check_if_branch_matches(pattern)
  return if ENV['CI']
  rails_env = fetch(:rails_env)
  puts "\nChecking, if this is the branch matches #{pattern.to_s}"
  return if current_branch.match pattern
  puts "\nATTENTION: include a type (feat|fix|ref) and a ticket number in your branch prior deploying to #{rails_env}\n"
  exit(1)
end

def current_branch
  @branch ||= `git rev-parse --abbrev-ref HEAD`.chomp
end
